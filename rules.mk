# Copyright (C) 2022 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

MANIFEST := $(LOCAL_DIR)/manifest.json

MODULE_SRCS += \
	$(LOCAL_DIR)/lib.rs \

MODULE_CRATE_NAME := keymint

MODULE_LIBRARY_DEPS += \
	trusty/user/base/lib/hwbcc/rust \
	trusty/user/base/lib/hwkey/rust \
	trusty/user/base/lib/hwwsk/rust \
	trusty/user/base/lib/keymint-rust/boringssl \
	trusty/user/base/lib/keymint-rust/common \
	trusty/user/base/lib/keymint-rust/ta \
	trusty/user/base/lib/log-rust \
	trusty/user/base/lib/protobuf-rust \
	trusty/user/base/lib/storage/rust \
	trusty/user/base/lib/tipc/rust \
	trusty/user/base/lib/system_state/rust \
	trusty/user/base/lib/trusty-log \
	trusty/user/base/lib/trusty-std \

MODULE_RUSTFLAGS += \
	--cfg 'feature="soft_attestation_fallback"' \

MODULE_RUST_TESTS := true

include make/library.mk
